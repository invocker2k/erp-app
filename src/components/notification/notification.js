import { notification } from "antd";
notification.config({
  placement: "bottomLeft",
  bottom: 50,
  duration: 3,
});
const translateError = (err) => {
  if (err === "Request failed with status code 400") return "Lỗi đăng nhập";
  if (err === "Network Error") return "Lỗi kết nối";
  return err;
};

export function warning(msg) {
  notification.warning({
    message: msg,
  });
}

export function success(msg) {
  notification.success({
    message: msg,
  });
}

export function error(msg) {
  notification.error({
    message: "Có lỗi xảy ra",
    description: translateError(msg),
    style: {
      fontWeight: 400,
    },
  });
}

// export function FhdMessenger() {
//   var success = function (msg) {
//     notification.success(msg);
//   };
//   var error = function (msg) {
//     notification.error(msg);
//   };
// }
