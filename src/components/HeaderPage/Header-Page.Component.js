import React from "react";
import { PageHeader } from "antd";

const PageHeaderComponent = (props) => {
  return (
    <PageHeader
      style={{ border: "1px solid rgb(235, 237, 240)" }}
      onBack={props.cb}
      title={props.title}
      subTitle={props.subTitle}
    />
  );
};

// const style = {
//   page_header: {
//     border: "1px solid rgb(235, 237, 240)",
//   },
// };

export default PageHeaderComponent;
