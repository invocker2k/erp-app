import HomeView from "../views/HomeView/HomeView";
import LoginView from "../views/LoginView/LoginView";
import WelcomeView from "../views/WelcomeView/WelcomeView";
import ListProductsView from "../views/ListProductsView/ListProductsView";
import SaleEntryView from "../views/saleEntry/sale-entry.view";
// Some folks find value in a centralized route config.
// A route config is just data. React is great at mapping
// data into components, and <Route> is a component.

// Our route config is just an array of logical "routes"
// with `path` and `component` props, ordered the same
// way you'd do inside a `<Switch>`.
const routes = [
  {
    path: "/Wellcome",
    exact: true,
    component: HomeView,
  },
  {
    path: "/Login",
    exact: true,
    component: LoginView,
  },
  {
    path: "/home",
    exact: true,
    component: WelcomeView,
  },
  {
    path: "/products",
    exact: true,
    component: ListProductsView,
  },
  {
    path: "/saleEntry",
    exact: true,
    component: SaleEntryView,
  },
  //   {
  //     path: "/tacos",
  //     component: Tacos,
  //     routes: [
  //       {
  //         path: "/tacos/bus",
  //         component: Bus,
  //       },
  //       {
  //         path: "/tacos/cart",
  //         component: Cart,
  //       },
  //     ],
  //   },
];

export default routes;
