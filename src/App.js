import React, { useState, useEffect } from "react";
// import logo from "./logo.svg";
import "./App.css";
import MainContent from "./layouts/MainContent";
import { connect } from "react-redux";
import MainSider from "./layouts/MainSider";

import "antd/dist/antd.css";
import { Layout } from "antd";

const mapStateToProps = (state) => {
  return {
    state: state.AuthState,
  };
};

function App(state) {
  const [token, setTocken] = useState(false);
  useEffect(() => {
    if (localStorage.getItem("token")) setTocken(true);
  });
  return (
    <div
      style={{
        width: "100%",
        height: "100%",
      }}
    >
      <Layout style={{ minHeight: "100vh" }}>
        {token ? <MainSider /> : null}
        <Layout>
          <MainContent style={{ margin: "0 16px" }}></MainContent>
        </Layout>
      </Layout>
    </div>
  );
}

export default connect(mapStateToProps)(App);
