import axios from "axios";

const listProduct = async () => {
  try {
    const products = await axios.get("http://localhost:9999/products");
    console.log(products);
    return products;
  } catch (err) {
    return err;
  }
};

export { listProduct };
