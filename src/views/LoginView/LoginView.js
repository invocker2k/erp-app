import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
import { Form, Input, Button, Checkbox } from "antd";
import { UserOutlined, LockOutlined } from "@ant-design/icons";
import "antd/dist/antd.css";
import "./LoginView.css";
import { connect } from "react-redux";
import { LoginSuccess, LoginFailed } from "../../models/actions/AuthenAction";
import { getTokenByUser } from "../../api/UserApi";
import _ from "lodash";
import { error, warning } from "../../components/notification/notification";

const mapDispatchToProps = {
  LoginSuccess,
  LoginFailed,
};

const mapStateToProps = (state) => {
  return {
    state: state.AuthState,
  };
};

const LoginView = ({ state, LoginSuccess, LoginFailed }) => {
  const history = useHistory();

  const [token, setToken] = useState("");

  const onFinish = async (value) => {
    const user = {
      username: value.username,
      password: value.password,
      // remember: true
    };

    try {
      const [tokenApi] = await getTokenByUser(user);
      LoginSuccess({ tokenApi });
      setToken(tokenApi);
      return tokenApi
        ? history.push({
            pathname: "/home",
          })
        : null;
    } catch (err) {
      LoginFailed({ error: "err" });
    }
    error(
      "Tên tài khoản hoặc mật khẩu không chính xác. Nếu bạn quên mật khẩu, chọn forgot password để lấy lại mật khẩu"
    );
    return;
  };

  return (
    <div className="Login">
      <Form
        name="normal_login"
        className="login-form"
        initialValues={{ remember: true }}
        onFinish={onFinish}
      >
        <Form.Item
          name="username"
          rules={[{ required: true, message: "Please input your Username!" }]}
        >
          <Input
            prefix={<UserOutlined className="site-form-item-icon" />}
            placeholder="Username"
          />
        </Form.Item>
        <Form.Item
          name="password"
          rules={[{ required: true, message: "Please input your Password!" }]}
        >
          <Input
            prefix={<LockOutlined className="site-form-item-icon" />}
            type="password"
            placeholder="Password"
          />
        </Form.Item>
        <Form.Item>
          <Form.Item name="remember" valuePropName="checked" noStyle>
            <Checkbox>Remember me</Checkbox>
          </Form.Item>

          <a className="login-form-forgot" href="">
            Forgot password
          </a>
        </Form.Item>

        <Form.Item>
          <Button
            type="danger"
            htmlType="submit"
            shape="round"
            className="login-form-button"
          >
            Log in
          </Button>
          Or <a href="">register now!</a>
        </Form.Item>
      </Form>
    </div>
  );
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginView);
