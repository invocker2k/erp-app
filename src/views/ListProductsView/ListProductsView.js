import React from "react";
import "antd/dist/antd.css";
// import './index.css';
import { Table, Input, Button, Space } from "antd";
import Highlighter from "react-highlight-words";
import { SearchOutlined } from "@ant-design/icons";
import Header from "../../components/HeaderPage/Header-Page.Component";
import { listProduct } from "../../api/ProductApi";
import {
  warning,
  success,
  error,
} from "../../components/notification/notification";

class ListProductView extends React.Component {
  constructor() {
    super();
    this.state = {
      searchText: "",
      searchedColumn: "",
      hasProducts: "",
      data: [],
    };
  }

  async componentDidMount() {
    try {
      if (!localStorage.getItem("products")) {
        const products = await listProduct();
        if (products) {
          warning("Đang cập nhật danh sách sản phẩm");
          localStorage.setItem("products", JSON.stringify(products.data.row));
          this.state.hasProducts = true;
        }
        this.setState({
          data: JSON.parse(localStorage.getItem("products")),
        });
        success("Lấy products thành công !");
      } else {
        this.setState({
          data: JSON.parse(localStorage.getItem("products")),
        });
        success("Lấy products thành công !");
      }
    } catch (err) {
      error(`Lấy products thất bại !, ${err}`);
    }
  }
  getColumnSearchProps = (dataIndex) => ({
    filterDropdown: ({
      setSelectedKeys,
      selectedKeys,
      confirm,
      clearFilters,
    }) => (
      <div style={{ padding: 8 }}>
        <Input
          ref={(node) => {
            this.searchInput = node;
          }}
          placeholder={`Search ${dataIndex}`}
          value={selectedKeys[0]}
          onChange={(e) =>
            setSelectedKeys(e.target.value ? [e.target.value] : [])
          }
          onPressEnter={() =>
            this.handleSearch(selectedKeys, confirm, dataIndex)
          }
          style={{ width: 188, marginBottom: 8, display: "block" }}
        />
        <Space>
          <Button
            type="primary"
            onClick={() => this.handleSearch(selectedKeys, confirm, dataIndex)}
            icon={<SearchOutlined />}
            size="small"
            style={{ width: 90 }}
          >
            Search
          </Button>
          <Button
            onClick={() => this.handleReset(clearFilters)}
            size="small"
            style={{ width: 90 }}
          >
            Reset
          </Button>
        </Space>
      </div>
    ),
    filterIcon: (filtered) => (
      <SearchOutlined style={{ color: filtered ? "#1890ff" : undefined }} />
    ),
    onFilter: (value, record) =>
      record[dataIndex]
        ? record[dataIndex]
            .toString()
            .toLowerCase()
            .includes(value.toLowerCase())
        : "",
    onFilterDropdownVisibleChange: (visible) => {
      if (visible) {
        setTimeout(() => this.searchInput.select(), 100);
      }
    },
    render: (text) =>
      this.state.searchedColumn === dataIndex ? (
        <Highlighter
          highlightStyle={{ backgroundColor: "#ffc069", padding: 0 }}
          searchWords={[this.state.searchText]}
          autoEscape
          textToHighlight={text ? text.toString() : ""}
        />
      ) : (
        text
      ),
  });

  handleSearch = (selectedKeys, confirm, dataIndex) => {
    confirm();
    this.setState({
      searchText: selectedKeys[0],
      searchedColumn: dataIndex,
    });
  };

  handleReset = (clearFilters) => {
    clearFilters();
    this.setState({ searchText: "" });
  };

  render() {
    const columns = [
      // {
      //   title: "id",
      //   dataIndex: "id",
      //   key: "id",
      //   width: "10%",
      //   ...this.getColumnSearchProps("id"),
      // },
      {
        title: "sku",
        dataIndex: "sku",
        key: "sku",
        width: "20%",
        ...this.getColumnSearchProps("sku"),
      },
      {
        title: "name",
        dataIndex: "name",
        key: "name",
        width: "40%",
        ...this.getColumnSearchProps("name"),
      },
      {
        title: "price",
        dataIndex: "price",
        key: "price",
        width: "20%",
        ...this.getColumnSearchProps("price"),
      },
      {
        title: "salePrice",
        dataIndex: "salePrice",
        key: "salePrice",
        width: "20%",
        ...this.getColumnSearchProps("salePrice"),
      },
    ];
    return (
      <>
        <Header title="List Product" subTitle="All products invocker"></Header>
        <Table columns={columns} rowKey="sku" dataSource={this.state.data} />;
      </>
    );
  }
}

export default ListProductView;
