import React from "react";
import "./HomeView.module.css";
import { Button } from "antd";
import "antd/dist/antd.css";
import { Link } from "react-router-dom";

const HomeView = () => {
  return (
    <>
      <div className="App" style={{ ...style.HomeView, ...style.textColor }}>
        <div className="App-header">
          <div style={style.wellComeText}>WELLLCOME </div>
          <div style={style.wellComeText}>TO</div>
          <div style={style.wellComeText}>INVOCKER WORK</div>

          <div style={{ ...style.textColor }}>
            INVOCKE - <small>Hải sản & Đồ ăn biển</small>
          </div>
          <p style={{ ...style.textColor }}>
            Trang web order dành cho nhân viên
          </p>
          <Link to="/Login">
            <Button onClick={() => {}} type="danger" shape="round" size="large">
              Đăng nhập
            </Button>
          </Link>
        </div>
      </div>
    </>
  );
};

const style = {
  HomeView: {
    backgroundColor: "#CCFFFF",
  },
  wellComeText: {
    fontSize: 60,
    color: "grey",
  },
  textColor: {
    color: "grey",
  },
};
export default HomeView;
