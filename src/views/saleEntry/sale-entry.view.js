import React, { useState } from "react";
import "antd/dist/antd.css";
import Header from "../../components/HeaderPage/Header-Page.Component";
import _ from "lodash";
import { Button, Row, Col, Typography, AutoComplete, Table } from "antd";
const { Text } = Typography;

let productDescription = [
  { value: "Chưa có product nào cả, bạn thử cập nhật product nhé?" },
];

if (localStorage.products) {
  productDescription = [];
  let products = JSON.parse(localStorage.products);
  _.forEach(products, (product) => {
    productDescription.push({ value: product.name });
  });
}

const columns = [
  {
    title: "STT",
    dataIndex: "stt",
    key: "stt",
  },
  {
    title: "SKU",
    dataIndex: "sku",
    key: "sku",
  },
  {
    title: "Miêu tả",
    dataIndex: "name",
    key: "name",
  },
  {
    title: "Số lượng",
    dataIndex: "amount",
    key: "amount",
  },
  {
    title: "Đơn giá",
    dataIndex: "price",
    key: "price",
  },
  {
    title: "Giảm giá",
    dataIndex: "salePrice",
    key: "saleprice",
  },
  {
    title: "Thành tiền",
    dataIndex: "intoMoney",
    key: "intoMoney",
  },
];

const WelcomeView = () => {
  const [dataSource, setDataSource] = useState([]);

  const onSelect = (data) => {
    // data = data.split(" ")[0];
    const product = _.find(
      JSON.parse(localStorage.getItem("products")),
      (product) => product.name === data
    );
    setDataSource([
      ...dataSource,
      {
        stt: dataSource.length + 1,
        sku: product.sku,
        name: product.name,
        price: product.price,
        salePrice: product.salePrice ? product.salePrice : 0,
      },
    ]);
  };
  return (
    <>
      <Header title="List Product" subTitle="all of products" />
      <Row justify="center">
        <Col span={6}>
          <Text>Tìm sản phẩm</Text>
        </Col>
        <Col span={18}>
          <AutoComplete
            style={{
              width: "100%",
            }}
            options={productDescription}
            onSelect={onSelect}
            placeholder="Tìm sản phẩm"
            filterOption={(inputValue, option) =>
              option.value.toUpperCase().indexOf(inputValue.toUpperCase()) !==
              -1
            }
          />
        </Col>
      </Row>
      <Row>
        <Table
          style={{ width: "100%" }}
          dataSource={dataSource}
          columns={columns}
          rowKey="sku"
        />
      </Row>
      <Row>
        <Col>Tổng đơn hàng</Col>
        <Col></Col>
      </Row>
      <Row> Hoàn trả đơn hàng </Row>
      <Row>Tổng tiền hàng:</Row>
      <Row justify="center">
        <Button type="primary">Thanh toán</Button>
      </Row>
    </>
  );
};

const style = {};
export default WelcomeView;
