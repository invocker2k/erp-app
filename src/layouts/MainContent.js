import React from "react";
import routes from "../config/router";
import { connect } from "react-redux";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

const mapStateToProps = (state) => {
  return {
    state: state.AuthState,
  };
};

const MainContent = (state) => {
  return (
    <>
      <Router>
        <Switch>
          <Redirect path="/" exact to="/Wellcome" />
          {routes.map((route) => (
            <Route
              key={route.path || "N/A"}
              exact={route.exact}
              path={route.path}
              component={route.component}
            />
          ))}
        </Switch>
      </Router>
    </>
  );
};

export default connect(mapStateToProps)(MainContent);
