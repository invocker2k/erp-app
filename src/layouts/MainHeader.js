import React from "react";

// import routes from "./router";

import "antd/dist/antd.css";
import { Layout, Menu, Breadcrumb } from "antd";
const { Header } = Layout;
// import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

const PageHead = () => {
  return (
    // <Router>
    <Header value="small" className="header">
      <div className="logo" />
      <Menu theme="dark" mode="horizontal" defaultSelectedKeys={["2"]}>
        <Menu.Item key="1">nav 1</Menu.Item>
        <Menu.Item key="2">nav 2</Menu.Item>
        <Menu.Item key="3">nav 3</Menu.Item>
      </Menu>
    </Header>
    // </Router>
  );
};

export default PageHead;
