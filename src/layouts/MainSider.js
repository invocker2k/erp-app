import React, { useState } from "react";
import { useHistory, BrowserRouter as Router } from "react-router-dom";

import { Layout, Menu } from "antd";
import {
  DesktopOutlined,
  PieChartOutlined,
  TeamOutlined,
  UserOutlined,
  ReconciliationOutlined,
} from "@ant-design/icons";

const { Sider } = Layout;
const { SubMenu } = Menu;

const MainSider = () => {
  let history = useHistory();
  console.log(history);
  const [collapsed, setCollapsed] = useState(false);

  const onCollapse = () => {
    setCollapsed(!collapsed);
  };

  const logout = () => {
    localStorage.removeItem("token");
  };
  return (
    <Router>
      <Sider collapsible collapsed={collapsed} onCollapse={onCollapse}>
        <div className="logo" />
        <Menu theme="dark" defaultSelectedKeys={["1"]} mode="inline">
          <Menu.Item key="1" icon={<PieChartOutlined />}>
            Option 1
          </Menu.Item>
          <Menu.Item key="2" icon={<DesktopOutlined />}>
            Option 2
          </Menu.Item>
          <SubMenu key="sub1" icon={<UserOutlined />} title="User">
            <Menu.Item key="3">Tom</Menu.Item>
            <Menu.Item key="4">Bill</Menu.Item>
            <Menu.Item key="5">Alex</Menu.Item>
          </SubMenu>
          <SubMenu key="sub2" icon={<TeamOutlined />} title="Products">
            <Menu.Item key="6">
              <a href="/products" className="nav-text">
                List Products
              </a>
            </Menu.Item>
            <Menu.Item key="8">Create Products</Menu.Item>
          </SubMenu>
          <SubMenu key="9" icon={<ReconciliationOutlined />} title="Sale">
            <Menu.Item key="6">
              <a href="/saleEntry" className="nav-text">
                Sale Entry
              </a>
            </Menu.Item>
          </SubMenu>
          <Menu.Item key="1" icon={<UserOutlined />} onClick={logout}>
            <a href="/login" className="nav-text">
              Logout
            </a>
          </Menu.Item>
        </Menu>
      </Sider>
    </Router>
  );
};

export default MainSider;
