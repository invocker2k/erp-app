import store from "./stores/root-store";
import { addAuth } from "./actions/auth-action";

window.store = store;
window.addAuth = addAuth;
