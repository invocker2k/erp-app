const initState = {
  auth: {
    username: "",
    password: "",
    isAuth: false,
  },
};

function rootReducer(state = initState, action) {
  if (action.type === "ADD_AUTH") {
    state.auth = {
      ...state,
      ...action.payload,
    };
  }
  return state;
}

export default rootReducer;
