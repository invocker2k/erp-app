import { actionTypes } from "../../constrants/constrants";

const initState = {
  token: "",
  error: "",
};

function AuthReducer(state = initState, action) {
  switch (action.type) {
    case actionTypes.LOGIN_FAILED:
      return {
        ...state,
        error: action.payload.error,
      };
    case actionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        token: action.payload.tokenApi,
      };
    default:
      return state;
  }
}
export default AuthReducer;
