import constrants from "../../constrants/constrants";

export function LoginSuccess(payload) {
  localStorage.setItem("token", payload);
  return {
    type: constrants.actionTypes.LOGIN_SUCCESS,
    payload,
  };
}

export function LoginFailed(payload) {
  return {
    type: constrants.actionTypes.LOGIN_FAILED,
    payload,
  };
}

// export default {LoginSuccess,LoginFailed}
