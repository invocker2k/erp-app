import { createStore, combineReducers } from "redux";
import rootReducer from "../reducers/index";
import AuthReducer from "../reducers/AuthReducer";
const reducer = combineReducers({
  auth: rootReducer,
  AuthState: AuthReducer,
});

const store = createStore(reducer);

export default store;
